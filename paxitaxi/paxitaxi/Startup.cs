﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(paxitaxi.Startup))]
namespace paxitaxi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
